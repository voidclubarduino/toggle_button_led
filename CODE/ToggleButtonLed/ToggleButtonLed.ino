/**
 * Toggle Button Led
 * Liga e desliga o LED em função do clique de um botão.
 * Neste exemplo é utilizado o LED EXTERNO ligado ao pino 13 e um botão com configuração PULL-UP ligado ao pino 4.
 * Cada clique no botão altera o estado do LED. Se o LED se encontrar ligado, um clique do botão altera o seu estado para desligado. 
 * Caso o LED se encontre desligado, um clique do botão altera o seu estado para ligado. 
 *
 * Devido a problemas a nível mecânico e físico, por vezes, os botões geram ruído, o qual, se não for tratado,
 * pode levar a que os programas assumam múltiplos cliques do botão quando o mesmo apenas foi clicado uma única vez.
 * O tratamento do ruído denomina-se debouncing. Este exemplo inclui a programação desse mesmo debouncing.
 *
 * criado a 28 de Abril 2016
 * por Bruno Horta, Ivo Oliveira
*/
#define LED_PIN 13
#define BUTTON_PIN 4
/**
 * Variáveis necessárias para debouncing.
 */
int buttonState;             
int lastButtonState = LOW;   
long lastDebounceTime = 0;
/**
 * DEBOUNCE DELAY
 */
long debounceDelay = 50;

/**
 * Estado do LED
 */
int ledState = LOW;
void setup() {
  Serial.begin(9600);
  /*
   * Definir o modo de funcionamento do botão
  */
  pinMode(BUTTON_PIN, INPUT);
  /*
   * Definir o modo de funcionamento do LED
  */
  pinMode(LED_PIN, OUTPUT);
  /**
   * inicializar o pino relativo ao Led com o estado inicial do Led. No nosso exemplo, o Led iniciar-se-à desligado.
   */
  digitalWrite(LED_PIN, ledState);
}
void loop() {
  /**
   * Verificar se a leitura é válida ou se se trata de ruído
   */
  if(processDebounce(BUTTON_PIN)){
    /**
     * Visto a leitura ser válida,
     * Inverter o estado do LED
     */
    ledState = !ledState;
    /**
     * 
     * ativar o pino usando a função:
     *  digitalWrite(PINO[1-13],VALOR[HIGH,LOW])
     *   HIGH = 1 = 5volts --> LIGADO
     *   LOW = 0 = 0volts --> DESLIGADO
     * 
    */
    digitalWrite(LED_PIN, ledState);
  }
}
/**
 * Realiza uma leitura para um determinado pino e devolve true caso a leitura corresponda ao estado atual 
 * do botão associado ao pino e a leitura indique que o pino se encontra ativo (estado HIGH). 
 * Em qualquer outra situação a função retorna false, seja por o botão não se encontrar ativo, 
 * seja por a leitura ser considerada ruído (uma vez que o tempo decorrido desde a última leitura considerada válida 
 * não é superior ao intervalo de debouncing);
 */
boolean processDebounce(int pin){
  // ler o estado do pino para uma variável local
  int sensorVal = digitalRead(pin);
  /*
   * verificar se o botão foi pressionado e o tempo que decorreu desde o último pressionar do botão 
   * é suficiente para ignorar qualquer tipo de ruído.
  */

  /*
   * Se a leitura registou uma alteração de estado, seja ele ruído ou o pressionar do botão
  */
  if (sensorVal != lastButtonState) {
    // reiniciar o contador de debouncing
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    /**
     * Qualquer que seja a leitura, esta aconteceu a um tempo superior ao intervalo 
     * de debouncing considerado (no exemplo 50 milisegundos).
     * Por essa razão, pode se assumir a leitura como sendo o estado atual do botão.
     */
    if (sensorVal != buttonState) {
      /*
       * o estado atual do botão é diferente ao último estado válido registado, por isso,
       * igualar o último estado válido, para o botão, como sendo a leitura atual.
       */
      buttonState = sensorVal;
      if (buttonState == HIGH) {
        /*
         * definir o último estado lido, para o botão, como sendo a leitura atual.
         */
        lastButtonState = sensorVal;
        /**
          * O botão encontra-se ativo, por isso retorna-se true
        */
        return true;
      }
    }
  }
  /*
   * definir último estado lido, para o botão, como sendo a leitura atual.
   */
  lastButtonState = sensorVal;
  return false;
}



